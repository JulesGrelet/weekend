// classe Personne
public class Personne {


    // attributs
    private String nom;


    // constructeur
    /**
     * constructeur de la classe Personne
     * 
     * @param leNom String nom de la personne
     * 
     * @author JulesGrelet
     */
    public Personne (String leNom) {
        this.setNom(leNom);
    }


    // getteurs
    /**
     * fonction getNom()
     * getteur de nom : renvoi le nom de la personne
     * 
     * @return String nom de la personne
     * 
     * @author JulesGrelet
     */
    public String getNom () {return this.nom;}

    // setteurs
    /**
     * méthode setNom()
     * setteur de nom : défini le nom de la personne
     * 
     * @param leNom String nom de la personne
     * 
     * @author JulesGrelet
     */
    public void setNom (String leNom) {this.nom=leNom;}

    
}
