public class Executable {
    
    public static void main(String[] args){

        // nouveaux weekends
        Weekend superWE = new Weekend("Super weekend");

        // nouvelles personnes
        Personne pierre = new Personne("Pierre");
        Personne paul = new Personne("Paul");
        Personne marie = new Personne("Marie");
        Personne anna = new Personne("Anna");


        // ajout de personnes à des weekends
        superWE.ajoutePersonne(pierre);
        superWE.ajoutePersonne(paul);
        superWE.ajoutePersonne(marie);
        superWE.ajoutePersonne(anna);

        // ajout de dépenses à des weekends
        superWE.ajouteDepense(pierre, 12, "pain");
        superWE.ajouteDepense(paul, 100, "pizzas");
        superWE.ajouteDepense(pierre, 70, "essence");
        superWE.ajouteDepense(marie, 15, "vin");
        superWE.ajouteDepense(paul, 10, "vin");


        // affiche le montant de dépenses totales d'un weekend
        System.out.println(superWE.totalDepenses()); //207.0

        // affiche le montant de dépenses d'un weekend pour une personne
        System.out.println(superWE.totalDepensesPersonne(pierre)); //82.0
        System.out.println(superWE.totalDepensesPersonne(paul)); //110.0
        System.out.println(superWE.totalDepensesPersonne(anna)); //0.0

        // affiche le montant de la moyenne de dépenses d'un weekend
        System.out.println(superWE.depenseMoyenne()); //51.75

        // affiche le montant à payer ou recevoir pour une personne par equilibrage des dépenses d'un weekend
        System.out.println(superWE.equilibrer(marie)); //-36.75
        System.out.println(superWE.equilibrer(paul)); //51.75

        // affiche le montant total de dépenses d'un weekend pour un article donné (libelle)
        System.out.println(superWE.depenseProduit("pizzas")); //100.0
        System.out.println(superWE.depenseProduit("vin")); //25.0
        System.out.println(superWE.depenseProduit("cereales")); //0.0


        // affiche les détail du weekend
        System.out.println(superWE); //toString()

    }

}
