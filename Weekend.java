// classes import
import java.util.List;
import java.util.ArrayList;

// classe Weekend
public class Weekend {
    

    // attributs
    private String nom;
    private List<Depense> lesDepenses;
    private List<Personne> lesPersonnes;


    // constructeur
    /**
     * constructeur de la classe Weekend
     * 
     * @param leNom String nom du weekend
     * 
     * @author JulesGrelet
     */
    public Weekend (String leNom) {
        this.setNom(leNom);
        this.lesDepenses = new ArrayList<Depense>();
        this.lesPersonnes = new ArrayList<Personne>();
    }


    // getteurs
    /**
     * fonction getNom()
     * getteur de nom : renvoi le nom du weekend
     * 
     * @return String nom du weekend
     * 
     * @author JulesGrelet
     */
    public String getNom () {return this.nom;}
    /**
     * fonction getLesDepenses()
     * getteur de lesDepenses : renvoi la liste des dépenses du weekend
     * 
     * @return ArrayList[Depense] dépenses du weekend
     * 
     * @author JulesGrelet
     */
    public List<Depense> getLesDepenses () {return this.lesDepenses;}
    /**
     * fonction getLesPersonnes()
     * getteur de lesPersonnes : renvoi la liste des personnes du weekend
     * 
     * @return ArrayList[Personne] personnes du weekend
     * 
     * @author JulesGrelet
     */
    public List<Personne> getLesPersonnes () {return this.lesPersonnes;}

    // setteurs
    /**
     * méthode setNom()
     * setteur de nom : défini le nom du weekend
     * 
     * @param leNom String nom du weekend
     * 
     * @author JulesGrelet
     */
    public void setNom (String leNom) {this.nom=leNom;}


    // méthodes
    /**
     * méthode ajoutePersonne()
     * ajoute une personne au weekend si elle n'y est pas déjà
     * 
     * @param laPersonne Personne personne à ajouter au weekend
     * 
     * @author JulesGrelet
     */
    public void ajoutePersonne (Personne laPersonne) {
        // if contains() pour éviter d'avoir deux mêmes personnes
        // dans un weekend
        if (this.lesPersonnes.contains(laPersonne) == false)
            this.lesPersonnes.add(laPersonne);
    }

    /**
     * méthode ajouteDepense()
     * ajoute une Depense au weekend
     * 
     * @param laPersonne Personne personne ayant fait la depense
     * @param leMontant double montant de la dépense
     * @param leLibelle String libelle/nom de la dépense
     * 
     * @author JulesGrelet
     */
    public void ajouteDepense (Personne laPersonne, double leMontant, String leLibelle) {
        // pas de if contains() ici car dans un weekend une même
        // personne peut faire plusieurs mêmes dépenses au même prix
        this.lesDepenses.add(new Depense(laPersonne, leMontant, leLibelle));
    }

    /**
     * fonction totalDepensesPersonne()
     * renvoi le montant total des dépenses du weekend pour une personne
     * 
     * @param laPersonne Personne personne ayant fait la ou les dépense(s)
     *
     * @return double montant total des dépenses d'une personne
     * 
     * @author JulesGrelet
     */
    public double totalDepensesPersonne (Personne laPersonne) {
        double total = 0;
        for (Depense laDepense : this.lesDepenses) {
            if (laDepense.getLaPersonne() == laPersonne)
                total += laDepense.getMontant();
        }
        return total;
    }

    /**
     * fonction totalDepenses()
     * renvoi le montant total des dépenses du weekend
     * 
     * @return double montant total des dépenses
     * 
     * @author JulesGrelet
     */
    public double totalDepenses () {
        double total = 0;
        for (Depense laDepense : this.lesDepenses) {
            total += laDepense.getMontant();
        }
        return total;
    }

    /**
     * fonction depenseMoyenne()
     * renvoi le montant moyen des dépenses du weekend par rapport au nombre de personnes
     * 
     * @return double montant moyen des dépenses
     * 
     * @author JulesGrelet
     */
    public double depenseMoyenne () {
        double moy = 0;
        for (Depense laDepense : this.lesDepenses) {
            moy += laDepense.getMontant();
        }
        moy /= this.getLesPersonnes().size();
        return moy;
    }

    /**
     * fonction equilibrer()
     * renvoi le montant qu'une personne doit donner ou recevoir pour equilibrer les dépenses du weekend
     * <ul>
     *  <li>x inférieur à 0 : la personne donne -x euros</li>
     *  <li>x égal à 0 : la personne ne donne rien et ne recois rien</li>
     *  <li>x supérieur à 0 : la personne recoit x euros</li>
     * </ul>
     * 
     *  @param laPersonne Personne personne à equilibrer les dépenses
     * 
     * @return double montant à donner/recevoir
     * 
     * @author JulesGrelet
     */
    public double equilibrer (Personne laPersonne) {
        double totalPersonne = 0;
        for (Depense laDepense : this.lesDepenses) {
            if (laDepense.getLaPersonne() == laPersonne)
                totalPersonne += laDepense.getMontant();
        }
        double equilibrage = totalPersonne - this.depenseMoyenne();
        // equilibrage<0 : la personne doit donner (equilibrage*-1) euro(s)
        // equilibrage==0 : la personne ne donne rien et ne recois rien
        // equilibrage>0 : la personne doit recevoir (equilibrage) euro(s)
        return equilibrage;
    }

    /**
     * fonction depenseProduit()
     * renvoi le montant total des dépenses du weekend pour un produit (libelle)
     * 
     * @param leLibelle String libelle du produit des dépenses
     * 
     * @return double montant total des dépenses d'un produit
     * 
     * @author JulesGrelet
     */
    public double depenseProduit(String leLibelle) {
        int total = 0;
        for (Depense laDepense : this.lesDepenses) {
            if (laDepense.getLibelle() == leLibelle)
                total += laDepense.getMontant();
        }
        return total;
    }

    /**
     * fonction personnePasDepense()
     * determine si une personne n'a pas depensé pour le weekend
     * 
     * @param laPersonne Personne personne choisie
     *
     * @return boolean true si la personne n'a pas depensé et false si elle a depensé
     * 
     * @author JulesGrelet
     */
    public boolean personnePasDepense(Personne laPersonne) {
        List<String> depPersNames = new ArrayList<String>();
        for (Depense dep : this.getLesDepenses()) {
            if (depPersNames.contains(dep.getLaPersonne().getNom()) == false)
                depPersNames.add(dep.getLaPersonne().getNom());
        }

        boolean bool = true;
        if (depPersNames.contains(laPersonne.getNom()))
            bool = false;

        return bool;
    }

    /**
     * fonction toString()
     * renvoi les détails du weekend
     * 
     * @return String détails écrits
     * 
     * @author JulesGrelet
     */
    public String toString() {
        String sPers = "";
        String entPers = "e";
        if (this.getLesPersonnes().size()>1) {
            sPers = "s";
            entPers = "ent";
        }
        
        String str = "Le weekend "+this.getNom()+"\n";
        str += "---------------------------------------\n";

        str += this.getLesPersonnes().size()+" personne"+sPers+" y particip"+entPers+" :\n";
        for (Personne pers : this.getLesPersonnes()) {
            str += "    - "+pers.getNom()+"\n";
        }
        str += "---------------------------------------\n";

        str += this.totalDepenses()+"€ y a été dépensé :\n";
        for (Depense dep : this.getLesDepenses()) {
            str += "    - "+dep+"\n";
        }
        str += "---------------------------------------\n";

        str += "Repartition des dépenses :\n";
        for (Personne pers : this.getLesPersonnes()) {
            if (this.personnePasDepense(pers) == false)
                str += "    - "+pers.getNom()+" a dépensé au total "+this.totalDepensesPersonne(pers)+"€\n";
            else
                str += "    - "+pers.getNom()+" n'a rien dépensé\n";  
        }
        str += "---------------------------------------\n";

        str += "Equilibrage des dépenses : \n";
        for (Personne pers : this.getLesPersonnes()) {
            double equilibrage = this.equilibrer(pers);
            if (equilibrage < 0)
                str += "    - "+pers.getNom()+" doit donner "+(equilibrage*(-1))+"€\n";
            else if (equilibrage == 0)
                str += "    - "+pers.getNom()+" ne doit rien donner et ne doit rien recevoir\n";
            else if (equilibrage > 0)
                str += "    - "+pers.getNom()+" doit recevoir "+equilibrage+"€\n";
        }
        str += "---------------------------------------\n";

        str += "Dépenses des produits :\n";
        List<String> produits = new ArrayList<String>();
        for (Depense dep : this.getLesDepenses()) {
            if (produits.contains(dep.getLibelle()) == false)
                produits.add(dep.getLibelle());
        }
        for (String prod : produits) {
            str += "    - au total, "+this.depenseProduit(prod)+"€ ont été dépensé pour l'article "+prod+"\n";
        }

        return str;
    }


}
