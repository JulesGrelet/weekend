// classe Depense
public class Depense {


    // attributs
    private double montant;
    private String libelle;
    private Personne laPersonne;


    // constructeur
    /**
     * constructeur de la classe Depense
     * 
     * @param unePersonne Personne personne ayant fait la depense
     * @param leMontant double montant de la dépense
     * @param leLibelle String libelle/nom de la dépense
     * 
     * @author JulesGrelet
     */
    public Depense (Personne unePersonne, double leMontant, String leLibelle) {
        this.setMontant(leMontant);
        this.setLibelle(leLibelle);
        this.setLaPersonne(unePersonne);
    }


    // getteurs
    /**
     * fonction getMontant()
     * getteur de montant : renvoi le montant de la dépense
     * 
     * @return double montant de la dépense
     * 
     * @author JulesGrelet
     */
    public double getMontant () {return this.montant;}
    /**
     * fonction getLibelle()
     * getteur de libelle : renvoi le libelle/nom de la dépense
     * 
     * @return String libelle/nom de la dépense
     * 
     * @author JulesGrelet
     */
    public String getLibelle () {return this.libelle;}
    /**
     * fonction getLaPersonne()
     * getteur de laPersonne : renvoi la personne ayant fait la depense
     * 
     * @return Personne personne ayant fait la depense
     * 
     * @author JulesGrelet
     */
    public Personne getLaPersonne () {return this.laPersonne;}

    //  setteurs
    /**
     * méthode setMontant()
     * setteur de montant : défini le montant de la dépense du weekend
     * 
     * @param leMontant double montant de la dépense
     * 
     * @author JulesGrelet
     */
    public void setMontant (double leMontant) {this.montant=leMontant;}
    /**
     * méthode setLibelle()
     * setteur de libelle : défini le libelle/nom de la dépense du weekend
     * 
     * @param leLibelle String libelle/nom de la dépense
     * 
     * @author JulesGrelet
     */
    public void setLibelle (String leLibelle) {this.libelle=leLibelle;}
    /**
     * méthode setLaPersonne()
     * setteur de laPersonne : défini la personne ayant fait la dépense
     * 
     * @param unePersonne Personne personne ayant fait la dépense
     * 
     * @author JulesGrelet
     */
    public void setLaPersonne (Personne unePersonne) {this.laPersonne=unePersonne;}


    // méthodes
    /**
     * fonction toString()
     * renvoi les détails de la dépense du weekend
     * 
     * @return String détails écrits
     * 
     * @author JulesGrelet
     */
    public String toString() {
        return this.getLaPersonne().getNom()+" a payé "+this.getMontant()+"€ pour l'article "+this.getLibelle();
    }

    
}
